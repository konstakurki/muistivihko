
---

*2017-11-03 00:42:43.767300*

# Testausta testausta mör/pk/plli soikoon

![audio/2017-11-03-00-42-43-731506.ogv](audio/2017-11-03-00-42-43-731506.ogv)

---

*2017-11-03 00:44:49.165698*

# Testi nro. 2

äsken meni ääkköset pieleen otsikossa. Josko nyt toimis paremmin.

---

*2017-11-03 00:51:55.253969*

Nyt kokeillaan sitte läppärillä. Katotaa toimiiko.

---

*2017-11-03 00:52:36.325150*

# PitHmm. Olinkohan mä liian nopee tän homman kaa? Pitäs varmaan yleisesti ottaen venaa sen verran että toi `git pull` kerkee käyä loppuun.

---

*2017-11-03 00:50:41.868400*

# Ny pelittää todella hyvin.

![audio/2017-11-03-00-50-41-776943.ogv](audio/2017-11-03-00-50-41-776943.ogv)

---

*2017-11-03 00:56:45.618929*

# Takasin läppärille.

Joo. Jos kokeilis vielä vaikka Python-koodiblokkia.
```python
While True:
    pass
```

---

*2017-11-03 01:03:01.015214*

Jou

---

*2017-11-03 01:04:45.895472*

# Jees jees jees.

Koitan tehä tästä robustii sillä tavalla että pystys käyttää vaikka internetti puuttuski.

---

*2017-11-03 01:06:11.128907*

# Testataa audio messagee

![audio/2017-11-03-01-06-11-093880.ogv](audio/2017-11-03-01-06-11-093880.ogv)

---

*2017-11-03 01:13:17.040572*

# Jees

Nyt vielä testataan “puhelimella” typewriteriä.

---

*2017-11-03 01:19:19.922204*

# Nyt kokeillaan ett/q voiko tähän pastettaa tekstii muualta.lopussa on audiojuttuja <= toi on pastetettu muualta

---

*2017-11-03 12:07:22.373628*

#        string = string.replace('\\',pholder)
 hmm?

mitäköhän tossa oikeen tapahttu? toimiiko backslash oikein? kokeillaan: \ (pitäs näkyy yhtenä backslashina), \q (pitäs näkyy backslash backslash q)


---

*2017-11-03 12:12:29.443748*

# Mikäs ihme toi ny oikeen oli

En tiiä prkl

jees

moro

---

*2017-11-03 12:12:40.258068*

# taas testit/q/qn

![audio/2017-11-03-12-12-40-211978.ogv](audio/2017-11-03-12-12-40-211978.ogv)

---

*2017-11-03 12:17:05.377733*

# Nyt harjotellaan kirjoittamista.

Nyt olis tarkoitus harjotella kirjoittamaan apple keyboardila sokkona github flaveored markdownia. Testataan ensimmäisenä että toimiiko urlin linkkaus ilman http://:aa: stallman.org. toivotaan että pelittää.

mitä vittua? tosta pitäs tulla bell character konsoliin eikä tekstiin mennä mitäqn.

;.;[--[;.['/[D\[D

okei joo päppäimistö tippu lattialle.

Pitää lopettaa että saa selvitettyy ton bell character jutun.

---

*2017-11-03 12:23:12.517791*

\bell
\bell
# Lisää harjotteluu

Edelleenkään ei bell character printtaa vaikka koodi kyllä näyttää siltä että totta kai sen pitäs toimii.

No, ihan sama. Treenataan gitlab flavored markdownii sokkona.

## Blockquotet

Blockquoteja voi kirjottaa parilla tavalla markdownissa. Perus tapa on laittaa `>` joka rivun alkuun.

> Siitä, mistä ei voi puhua, on vaiettava.
>
> — Wittgenstain joskus melkein sata vuotta sitten

Tossa blockquotesa pitäs olla pari paragraphia.

Sitten ainaki tässä gitlabissa on tää toinenki tyyli.
>>>
Jou, jou, jou.

Jou, jou.

Tähän tuli vielä ylimäqrqnen kappale.
>>>

Sitten koodin kirjottaminen.

## Koodi

Pikku paljoja koodii voi tekstin sekaan laittaa `laittamalla` backticksit sen ymp\rille. Koodiblokin voi tehä tälleen[C

```python
while True:
    pass
```
Siinoli highlightattu python-blokki. Kokeillaampa vielä TeXXiä.
```tex
\begin{equation}
f(x)=x^2
\end{equation}
```

## Kakkaemoji

Emojeja voi laittaa, mut ainoastaan kakkaemojilla on merkitystä. :poop: :shit:

## Linkkaukset muistivihon sisällä

* [yks rivi pythonia](typewriter.py)
* [jou](./typewriter.py)
* [johonki riviin[./typewriter.py
] nyt meni aiva vituiks

* [johonki riviin](./typewriter.py#L10)

## Matematiikka

Jonkunlaista LaTeX-tyyppistä juttua pystyy näköjään tällä kirjottamaan. *`\tan(x)`$ okei tohon tuli typo. Katotaan miten toi parseri sen tulkitsee.

Kokeillaan uuestaan: `$\tan(x)$`. Js sit uuelle riville yhtälö.
```math
\begin{equation
}
f(x)=x^2
äää
```
Meni pieleen. Otetaan uusiks.
```math
\begin{equation}
f(x)==x^2
\end{equation}
```
Joo, eiköhän tässä ollu tarpeekse `$\KaTeX$`ii.

## Header ID’t

[linkki](#jees)

[linkki2](#header-idt)

Niin joo, kokeillaas www-alkusta linkkiä. www.gnu.org.

## Taulukko

Mä kokeilin jo aikasemmin taulukkoa, mut se meni jotenki pieleen. Nyt kokeillaan uuestaan.

| Kylä    | Väkiluku |
| :------- | --------: |
| Tikkala  | 200       |
| Moksi    | 50        |
| Lohja    | 3000      |

Jospa tää toimis. Kokeillaampa vielä että tarviiko noitten columnien olla tässä sourcessa aina samalla tavalla.

| Kylä | Väkiluku |
| --- | --- |
| Tikkala | 200 |
| Moksi | 50 |

Toivottavasti toimii, ois helpompi kirjottaa.

Joo, tässäpä tää tais sitte olla.

---

*2017-11-03 13:25:17.044446*

# Läppäri testii

Kokeillaan toimiiko tässä bell character oikein.

\bell

jaa no voi perkule, olin käyttäny v'q'qr'q'q komentoo, eli komentoo `\bell` vaikka oikee on `\beep` :D no joo eipä siinä mitään, hyv
ä ettei ollu softassa vikaa.

---

*2017-11-03 13:23:13.907733*

# KaTeXin testailua

Ouraid, äsken men KaTeX vähän pieleen, en osannu laittaa delimiterejä oikein. Kokeillaas uusiks: $`\KaTeX`$ ton pitäs toimii oikein, ja tän: $`f(x)=x^2`$. Ja sit vielä yhtälö.
```math
f(x)=x^2
```

Ja joo, vois kokeilla vähä monimutkasempaaa yhtälöö.
```math
D_\alpha V_\beta=\{{_\alpha}^{\gamma}_{\beta}\}V^\gamma + \partial_\alpha V_\beta
```
Joo tossa pitäs olla kovariantin derivaatan määrittely suurinpiirteinoikein. Kohta näkee.

Kokeillaas vielä $`\KaTeX\$-koodia laittaa blokkiin.
```tex
\begin{equation}
    f(x) = 2x^2
\end{equation}
```
Joo, eiköhä testit ollu ny tästtä.

---

*2017-11-03 14:02:13.198375*

`~!@#$%^&*()_+=

joo. 5+2=3, 7-4=8, 5*6 = 7, 7^2=49

!#@$%^&*()_+=-`jou`

[]{}\|'";:,.<>/?

---

*2017-11-03 14:13:14.396130*

# Jees.

Katotaan että toimiiko potenssimerkki. Eli vaikka 2^32. Siis 2^32 on jotain miljardi ja vähän päälle.

Joo, oiskoha nyt hyvä. Jees vamaan.

---

*2017-11-03 14:16:10.718755*

# Uus yritys

Pitää kokeilla että miten sais potenssin jälkeen pisteen toimimaan mukavasti. Eli 2^32 . Nyt mä laitoin tohon spacen väliin, toivottavasti se toimii. Ja escape characteriakinvois kokeill: 2^32\…

Nonii, kNiin, piti testaa: |||||||Wngström.

---

*2017-11-03 14:20:42.174183*

Vielä uutta testii. Miksen mä saa escapetettuu pistettä? Kokeillaas\. Ja Ångström.

---

*2017-11-03 14:22:51.891067*

# perkele saatana

2^32\.

---

*2017-11-03 15:24:42.549067*

# Jou jou demonstraatio

Mulla on kivekset jalassa. Jea jea.

---

*2017-11-03 15:21:14.136218*

# demodemodemo

![audio/2017-11-03-15-21-14-095339.ogv](audio/2017-11-03-15-21-14-095339.ogv)

---

*2017-11-03 17:13:41.965549*

# Muistivihossa pitää olla sotkua

Mä mietin että alotan tän muistivihon alusta sitten kun oon saanu testattua että tää toimii. Se johti siihen että mietin kauheesti minkälainen ensimmäinen kirjotus olis, se kun näkyy siinä heti alussa ja ijotenki toimii introductiona siihen.

Sitte tajusin että toi testisotku on täydellinen ibtrofuction. Se tekee selväks että tää on muistivihko eikä mikään valmis julkasu tms.  Muistivihossa pitää olla sitä mitä päästä tylee eikä sitä piä suunnitella sillein niinku jotain blogikirjotusta.
/quit

---

*2017-11-03 19:38:44.422215*

# Muistivihko ratkaisee ongelmia

Mun päässä pulppuaa paljon asioita ja mä haluaisin saada niitä talteen. Se ei oo mitenkään itsestäänselvän helppoa. Mä oon koittanu kirjottaa ihan normaalilla tekstieditorilla muistiinpanoja, ja sitten oon kirjottanu postauksia facecebookkiin ja tehny youtube-videoita.

Näissä on kaikissa ongelmansa. Kun tekstieditoirlla väsäää tekstiä, tulee väistämättä editoitua liikaa ja stressattua siitä että tuleeko lopputuloksesta hyvä. Lisäks hajanaiset tekstitiedostot katoaa helposti tietokoneen (tai vaikka jonku git repon) syövereihin.

Tubessa ja facepäivityksissä on se ongelma, että niissä tulee väkisellä mietittyä että tajuaako muut juttua. Siitä tulee paineita ja epäluonnollisuutta, ja masennusta ku  käyt ilmi että ei muut tajunnu. Ja tietysti se että tavara  tallentuu jonnekki jättikorporaation serverifarmiin jollain suljetulla tavalla.

Muistivihko ratkasee nää kaikki ongelmat: editiontia ei tuu tehtyä koska se ei oo mahollista, ja koska muistivihon sisältöä ei lähtökohtasesti lue kukaan, ei too paineitakaan.  Ja tää mun muistivihko perustuu plain tekstiin git repossa, joten mä kontrolloin tätä hommaa ite aika pitkälti.


---

*2017-11-03 19:59:58.421105*

# Note-to-fucking-self

Näyttää siltä että pitää kirjottaa pari muistiinpanoa että pääsee luonnolliseen moodiin tän muistivihon kanssa..

Pari tärtkeetä huomiota:

* Mä kirjotan **itelleni**. Pointti on laittaa ideoita talteen, koska niitä ei kuitenkaan muista niin kristallinkirkkaasti myöhemmin. Tää ei oo blogi, ja se et joku muu lukee tätä on toissijasta (eikä sitä välttämättä tapahu ollenkaan). Joten Konsta, kirjota vaan semmosta miitä sun ittes täytyy tietää myöhemmin. Tää on selvästi aika saakelin hankalaa.
* En tiiä oliks mulla muita pointteja. Ei ois tarvinnu tehä listaa. Se on tän muistivihon hauskoja puolia että jos alottaa listan ni sit se on lista. Se on eksakti recordi siitä mitä päässä liikkuu.


---

*2017-11-03 23:57:25.841589*

# Kuinka maailmaa voisi muuttaa?

Mitä enemmän maailman menoa miettii ja tutkailee, sitä selvemmäksi käy, että melkein kaikki asiat tehdään aivan päin perseenreikää.  Mitenkä asiaan voisi vaikuttaa? Järkeilemällä, ja puhumalla järkeä, sitä voisi ajatella. Mutta se on väärä vastaus.

Ongelma on siin/q, että kun ihmiset käy ns. keskustelua, on logiikka ja järki siinä vain sivuosassa. Pääosan ajasta vie näkemysten ja mielipiteiden esittäminen ja niiden uskottavuudesta neuvotteleminen. Neuvottelu tapahtuu ilmein, äänenpainoin ja elein, tiedostamatta.

Tämä ei ole vika vaan ominaisuus. Me ollaan pohjimmiltamme heimolaisia, ja me ollaan sopeuduttu elämään ympäristössä, jossa loogisia teorioita ei juurikaan ole saatavilla—ja vaikka olisi, ei niiden soveltaminen onnistuisi kuitenkaan. Sellaisessa maailmassa on ehdottomasti toimivinta pelata elämänkokemuksen tuomilla viisauden palasilla ja jättää teoretisointi vähemmälle.

Jotkut ihmiset painottaa logiikkaa tavallista enemmän ja jättää sosiaalisuuden ja emotionaalisuuden vähemmälle neuvottelemisessa. Sellasia tyyppejä kutsutaan autistisiksi henkilöiksi. Kaikki tietää kuinka tällaset “burgerit” tietää ja ymmärtää jostakin asiasta kauheesti, mutta tekee käsittämättömän typeriä ratkasuja arkipäiväisessä elämässä omien teorioidensa johdattelemana.

Nyky[äivänä—ja mä tarkotan sitä aikaa, kun on ollut suuria valtarakenteita, joissa ihmiset ei tunne toisiaan henkilökohtaisesti, eli jotain 10000 vuotta tai jotain—on semmonen ongelma, että keskustelu asioista päättävien henkilöiden ja kansan väli\q on pitkälti yksipuoleista. Pääministeri puhuu televisiossa, eikä kukaan kansalainen voi kysyä, että mitä se oikeen puheillansa meinaa. Tilanne suosii sellasia tyyppekä jotka on hyviä puhumaan paskaa, koska kansa ei pysty saamaan kiinni nheitä paskanpuhumisesta niin helposti.

Jos ihmiset arviois poliitikkojen puheita sen perusteella kuinka hyvin ne käy järkeen, ei paskanpuhuminen kykenis menestymään. Silloin autistiset tyypit teorioineen sais huomiota, ja vaikka yksittäinen autisti tekee massiivisia virheitä, muodostais 200 autistia yhdessä kuitenkin todella toimivan eduskunnan. (Okei, edustajien autismi ei sais olla liian syvää jotta kommunikaatio pelais jotenkin ja flexibiliteettiä olis, mutta kumminkin.)

Kuten sanottua, tää on utopiaa. Jotta se ois mahollista, pitäs ihmisen DNA:ta muuttaa. Ja vaikka on olemassa vaikka mitä genetic engineeringiä yms., ei siitä ole mitään apua, koska ei kukaan tiedä että millaset muutokset genomissa sais aikaan järjen painotuksen lisää mistä aivotoiminnassa.

Tilanne on siis se, että meidän ajattelu ei sovellu tällasessa nykyaikasessa ihmiskunnassa elämiseen. Pieni vvähemmistä huijareita saa meidät toimimaan järjettöm\asti. NNe, jotka yrittää tuoda järkeä hommaan mukaan—mesimerkiks Piraattipuolue—jää huomaamatta, koska kansan antenni ei geneettisistä syistä oo virittyny niiden taajuudelle.

Tietysti me ei aivan täysin ignoorata logiikkaa, ja sen  takia maailma muuttuu pikku hiljaa kohti parempaa suuntaa. Demokratia, ihmisoikeudet jne. on uskomuksia, jotka johtaa parempaan lopputulokseen kuin niiden edeltäjät. Tällasissa vallankumouksissa oleellista on, että ne, jotka asiaa ajaa, pitää järjen ja logiikan roolin tarpeeks pienenä ja keskittyy pääasiassa näyttämään oman tunteensa paloa.

Paska juttu on se, että tässä ei oikeen oo aikaa tollaselle prosessille. Ilmastonmuutos, plutokratia ja ydinsota on liian lähellä. (niin ja digitaaliaddiktio ja yksityisyyden menetys, jotka kontribuoi ainaki plutokratiaan.) Mulla vaan on semmonen fiilis että ei tästä tuu paskaakaan, kaikki menee vituiks.

Mä nään kyllä mielessäni yhen ratkasun. SRiittäis nimittäin että jengi tajuais vaan yhen asian: sen että me ollaan heimolaisia. No, mun oma mielenlaatu menee sinne autistiseen suuntaan jonkun verran, ja tää taitaa olla niitä paikkoja missä tulee missattua jotain oleellista. Mulla on semmonen fiilis, että totta kai porukka tajuaa että me ei olla sopeuduttu tähän maailmaan kun vaan asiasta mainitsee, mutta mun fiilis on väärässä. Syyt tuli selitettyy jo tässä aikasemmin.

*Toivoa valaa hiukan se, että kirja nimeltä *SApiens: A Brief History of Humankind* (en muista kirjottajan nimeä, joka siis selostaa ihmisen evolutiivisen historian ja nykymaailman tokuttomuuden todella tyhjentävästi on käännetty 45 kielelle (tai jotain sinne päin). Ehkä porukka oikeesti voi tajuta tän jutun. Tai sit se on vaan jotenki viihdyttävä kirja. Vikee sanoo.

Vittu, tais tulla ylimääränen \*-merkki tohon kappaleeseen. No ei voi mitään, tällasta se elämä kirjotuskoneen kanssa on.

---

*2017-11-04 01:43:53.082988*

# On vain kuplia ja pyramidihuijauksia

Onko bitcoin pyramidihuijaus, tai kupla, joka odottaa puhkeamistaan, kysytään joskus. Jotta tähän kysymykseen saatas tokkua,  on mietittävä valuuttojen ja muiden ihmisten arvottamien asioiden luonnetta yleensä.

Bitcoinin, kullan euron, dollarinn, Microsoftin , Googlen *etc*. arvo perustuu uskomukseen,. Tuo on hyvin jännä uskomus, sillä sen mukaan muut ihmiset uskovat siihen. Säq arvostat euroja, koska kaupunki sun ympärillä arvostaa niitä kanssa.

Eri ihmiset arvostaa tällasia valuuttoja (joo lasketaan pörssiyhtiötkin valuutoiks tässä tapauksessa) eri verran. Tämä johtuu siitä, että kurssit muuttuu, ja toinen ennakoi kurssin (mikä siis tarkoittaa vaan sitä arvoa minkä  tyypillinen/keskimääränen ihminen valuutalle antaa) muutoksia eri tavalla kuin toinen.

Tää on aivan suorastaan triviaali asia: jos mä oletan että huomenna jengi arvostaa bitvoinia enemmän ku tänään, mä arvostan bitvoinia tänään enemmän kuin porukka keskimäärin. Siispä mä investoin bitvoiniin. Se vnostaa kurssia pikkusen (jengi näkee että joku arvostaa bitcoinia, ja se saa  jengin arvostamaan sitä ite). Vastaavasti, jos mä myyn, laskee kurssi. Tosin nää muutokset on niin pieniä, ettei niitä huomaa, mutta kun moni myy (tai ostaa) yhtäaikaa, heilahtaa kurssi ihan huomattavasti.

Tällälailla valuuttojen kurssit sitten pomppii ylös alas, riippuen siitä, miten ihmiset ennakoi toistensa arvostuksen, eli siis kkäytännössä toistensa ennakointityylinn, muutosta. Tää tekee systeemistä luonteeltaan kaoottisen,. Kurssien ennustaminen on tunnetusti vaikeeta.

Minkä tahansa valuutan arvvo odottaa romahdustaan. joku päivä porukka tajuaa että kulta on vaan kiiltäväq pehmeetä metallia, bittcoin vaan salausavainpareja ja hasheja, Windows on paskaa, abusiivista suljettua koodia, ja nykyttaide on paskaa. JJa tietysti että pankit menettää maksukykynsä. Jokainen valuutta on kupla, joka voi puhjeta millon vaan.

Tietysti nuo kuplat voi tehdä elinaikanansa jotakin h työtä. Kulta, euro ja dollari organisoi yhteiskuntaa, bitcoin taas kuluttaa sähq ja lämmittää planettaa.

Jokainen kupla on, lukuunottamatta mahdollista sivussa syntyvää hyödyllistä työtä, nollasummapeli. Jos teet voittoa bitcoinilla, joku muu häviää. YTyypillisesti  muutamat voittaa paljon ja loput hhäviää. Bill Gates on maailman rikkain mies.

Kupla on pyramidihuijaus, jos se on suunniteltu varta vasten rikastuttamaan muutamia ilman, että sivussa tulee mitään hyödyllistä työtä sivutuotteena. Aika saatanan moni firma menee tähän kategoriaan—joidenkin aikaansaama työ on itseasiassa pelkästään haitallista. Puhelinmyyjät on kohtuu  harmiton esimerkki, sotateollisuus taas on ikävämmästä päästä.

Bitcoin on siis kupla. Onko se pyramidihuijaus? Mä veikkaan, ett'ä ei ole. Mutta se ei ole varmaa. Satoshi Nakamoto, nimimerkki bitcoinin takana, omistaa 30% kaikista maailman bitcoineista, mikä on nykyään jo arvoltaan aika monta miljardia. Voi olla, että se on hukannu coininsa, mutta voi myös olla, että se vaan venailee jossain jja oottaa että se voi astella esiin maailma valtiaana. By the way kukaan ei tiedä kuka tai mikä Satoshi Nakamoto oikeesti on.

Pyramidihuijaus tai ei, mä en enää nykyään arvosta bitcoinia. Se on kupla, ja mä kattelen jokaisen kuplan elinkaarta pidemmälle. (Siinä ei kyllä välttämq\ttä oo mitään järkee.) Tai no saatana, on bitcoinissa nyt hyviäkin puolia. Meinasin vaan että valuutat ylipäänsä on aika saatanan tylsiä juttuja. Ei rahalla mitään jännää saa.

Mutta yhteiskuntana me ei voida välttää rahaa. Se muuttu mahottomaks sillon ku maanviljelys keksittiin. Ja jos jotain rahaa pitää jokatapauksessa käyttää, on sillon kyllä bitcoinin tyyliset kryptovaluutat monessa suhteessa ymieleenkiintonen ja potentiaalinen vaihtoehto.

PS. Botcoin on teknisesti aivan *saatanan* elegantti ja kaunis systeemi.

---

*2017-11-04 11:14:18.527838*

# Me ollaan kaikki lomalla

Kaikki tietää, että nykymaailmassa on vaikee läytää paikkaansa ja masennusta on paljon. Mä luulen, että tää johtuu siitä, että nykymaailmassa meille ihmisille ei ole paikkoja, ainakaan semmosia paikkoja mitä me voitas luontomme puolesta täyttää, tarpeeksi. (Voi olla, että niitä ei oo melkein kellekään.) Tää on aika harvinainen näkemys. Se johtunee siitä, että onhan se nyt aivan perseestä olla sitä mieltä että omaa paikkaa on turha lähteä etsimään.

Se maailma, josta me paikkamme löydetään, on hheimoyhteiskunta, jossa ei ole muodollista rakennetta, heimon koko rajottuu sen mukaan kuinka paljon yks tyyppi voi tuntea ihmisiä henkilökohtasesti (~150, ns. Dunbarin luku), ja jossa ruuan hankkimiseen on olemassa aivan välitön motivaatio koko ajan. Hedelmiä on kerättävä ja kauriita keihästettävä, koska muuten tulee saatananmoinen nälkä.

Lienee aika selvää, että nykypäivän maailma poikkeaa tuosta aika paljon. Vaistojen seuraaminen johtaa ylipainoon, ylikansotukseen ja sotaan. . Monesti sanotaan, että maailma ei ole valmis, mutta se on kyllä paskapuhetta. Maailma on ollut valmis jo 10000 vuotta, ja “we’re a species on a holiday,” kuten Pieter Hintjens kirjottti kirjaansa http://psychopathcode.com. (Suomeks se on että me ollaan laji joka on lomalla.)

On aika perseestä revitty ajatus että me ollaan toimettomia eikä mitään kannata tehdä. N Mä luulen, että se on aika perkeleen merkittävä syy siihen, että ihmiset uskoo kaikenlaisiin hömpötyksiin, kuten vaikkapa kuolemanjälkeiseen elämään tai talouskasvuun. Kun ei oo mitään oikeeta tekemistä, on pakko keksiä uskomuksia, jotta ei vaipuis masennukseen.

Tietysti on jotain mitä meiän varmaan kannattas tehä. Vaikkapa ympäristön tuhoutumisen estäminen. Mutta se ei ole semmosta duunia, mitä me osattas tehdä vaistonvarasesti, ja sen takia se menee aivan päin vittua. Öljyä vaan poltetaan ja poltetaan, eikä poliittisesta päätöksenteosta tuu mitään.

Kerran jkun juttelin yhen uskovaisen tyypin kanssa, se kysy: “Jos ei ois jumalaa, minkä takia mä nousisin aamulla ylös sängystä?” Nyt mä oon vasta alkanu tajuta miitä se oikeen meinas. Mä en usko enää oikeen mihinkään, ja perkele, ei todellakaan oo itsestään selvää että miks nousta sängystä ylös. Joskus tuleekin pötkötettyä koko päivä sängyssä saatana.

No, on yks uskomus, johon mä aina välilä onnistun uskomaan, ja joka suojelee mua masennukselta sillon tällön. Se on, että mä saisin jeesattua porukkaa tajuamaan tämän asian ytimen—siis sen, että me ollaan heimoeläimiä, ja yleisemmin ottamaan evolutiivisen näkökulman elämän fundeeraamiseen. Aika päin vittua on tähän mennessä menny, mutta ehkä sitä joskus sais löydettyä semmosia sanoja joita porukka vois ymmärtää.

Onhan toi ihan naurettava ajatus. Evoluutiopsykologia ite antaa vahvoja viitteitä siitä että kansa tuskin kiinnostuu evoluutiopsykologiasta. Se vaatii vammasuutta nimeltä autismi. Mut ihan vitun sama, uskon silti. On se ny vähemmän tyhmä uskomus ku vaikka pelastus (tai helvetinperkeleenreikä) kuoleman jälkeen, tai raha. Ja aina voi masentuu jos usko loppuu.

---

*2017-11-04 12:02:52.954134*

# Spinorit—mitä me tiedetään ja ei tiedetä

Spinorit on elintärkeä juttu teoreettiselle fysiikalle. Elektroneja ei voi kunnolla kuvata ilman niitä. Spinorit on geometrisia otuksia jotka elää monistolla. Vähän niinkun vektorit ja tensorit, mutta ei kuitenkaan ihan. Asia on pitkälti hämärän peitossa.

Vektorit on nuolia tai nuolimaisia otuksia moniston tangenttiavaruudessa. Nuolten laittaminen peräkkäin luo tangenttiavaruuteen luonnollisen yhteenlaskun, ja nuolen pituuden skaalaaminen numerolla skalaarikertolaskun. Vektorit siis muodostaa vektoriavaruuden (kuten nimestä vois päätellä). Tensorit on sitten lineaarikuvauksia vektoreilta R:lle. Tää on aika selvää ja intuitiivista. Spinoreista ei osata tä\llä lailla sanoa että mitä ne konkreettisesti on. Tai m\ en ainakaan osaa sanoa, eikä Wikkipedia.

Me tiedetään kuitenkin jotain spinoreiden numeerisen esityksen muuttumisesta kun koordinaatistoa vaihdetaan. Fyysikot määrrittelee spinorin juurikin näiden muutosominaisuuksien perusteella. Fyysikot ei välttämättä näe spinoreissa ongelmaa, koska fyysikot määrittelee vektorit ja tensoritkin usein vaan tällälailla koordinaattimuunnoksen kautta. Se riittää monesti laskujen tekemiseeen, mutta ymmärrys jää sillon väistämättä pinnalliseksi.

Jos meillä on monistolla joku koordinaatisto (atlas), indusoi se sillon jokaiseen tangenttiavaruuteen kannan. Kantavektorit on siis vaan koordinaattikenttien (ne on skalaarikenttiä) gradientit. Kun koordinaatistoa vaihdetaan, muuttuu vektorien komponenttiesitykset koordinaattimuunnoksen Jacobin matriisilla, mikä on GL(n):n alkio. Tensoorien numeeriset esitykset muuntuu sitten GL(n):n eri esitysten mukaan (Jacobin matriiseja ja sen käänteismatriiseja tulee indeksien mukaan sopivalla tavalla).

Me voidaan käyttää myös jotain muuta kantaa kuin moniston koordinaattien indusoimaa kantaa. Spinoreja varten tarvitaan (tai voi olla että ei tarvita, selitän kohta) ortonormaali kanta jokaiseen tangenttiavaruuden pisteeseen. Yleisessä tapauksessa tällasta sei saada moniston koordinaatiostosta. Mutta me voidaan tehdä n kappaletta vektorikenttiä jotka on ortonormalieja (ainakin pisteen ympäristössä) ja käyttää niitä tangenttiavaruuksien kantoina.

Kun ortonormaali kanta vaihdetaan toiseen, on kannat yhteydesssä toisiinsa SO(n):n elementeill\q, eli rotaatioilla. Rotaatio riippuu tietysti moniston pisteestä. Tensorit muuntuu SO(n):n eri esityksillä. Nyt ollaan jo lähellä spinoreita: spinori muuttuu SO(n):n ns. spinoriesityksillä, jotka on“kaksiarvoisia”. Mutta tää on  löysää puhetta, joten pitää selittää paremmin.

Kun rotaatio suoritetaan fyysisessä maailmassa, tapahtuu se jotakin polkua pitkin. Asento ei voi epä\jatkuvasti hypätä toiseksi asennoksi. Tämä tarkottaa sitä, että oikeestaan meidän pitää alottaa SO(n):n polkuavaruudesta.

Kaikista yleisin geometrinen objekti muistaa koko tuon polun, ja sen muunos on siis tuon polkuavaruuden alkio. En tiiä onko tollasilla otuksilla käyttöä missään. Seuraava vaihtoehto on, että otus muistaa polusta jotain, mutta ei välitä jos polkua muutetaan pikkusen (jatkuvastiasti). Tämä siis tarkottaa matemaattisesti sitä, että llaitetaan polkuavaruuteen ekvivalenssirelaatio, jossa kaks polkua on ekvivalentit jos niillä on samat päätepisteet ja ne voidaan deformoida jatkuvasti toisikseen. Käsittääkseni sanotaan, että polut kuuluu samaan homotopialuokkaan.

Oleellista on, että näitä homotopialuokkia *ei* voi karakterisoida pelkästään päätepisteet antamalla. Jokaista päätepisteparia vastaa kaksi polkua. Jos nämä  kaksi polkua vielä lätkäistään ekvivalenteiksi, pääästään SO(n):n. Tällöin menetetään ryhmän yhdesti yhtenäisyys. Sitä ryhmää, missä tuota jälkimmäist\q ekvivalentointia ei ole tehty, kutsutaan SO(n):n universal covering groupiksi. Esim. SO(3):n universal covering group on SU(2).

Spinorit on nyt otuksia, jotka muuntuu tuon universal covering groupin mukaan. Spinori siis muistaa paitsi rotaation päätepisteet, myös polun homotopiatyypin. Tämä  on kummallista, koska ei tule mieleen mitään juttua moniston pinnalla mikä voisi tuolla lailla käyttäytyä. Erittäin oleellista olisi keksiä jokin juttu joka sillä tavalla käyttäytyy. Se voisi antaa vinkkkiä siitä, kuinka muodostaa sellaista teoriaa spinoreista, mihin ei tarvitse sotkea kantavektoreita ja niiden muunnoksia muukaan.

Yks juttu kyllä tulee mieleen aidosta fysikaalisesta spinorista: otetaan vyö ja naulataan sen toinen pää kiinni maahan. Kun vyön toista päätä rotatoidaan, muistaa vyö polun homotopiatyypin, ja vyön tila muuntuu SU(2):n mukaan. Tätä ei varmaan voi ymmärtää tällä selityksellä, pitäs näyttää oikeella vyöllä. Tää kuulkee nimellä “Dirac’s belt trick”; Wikipediasta voi kattoo ja varmaan juutuubista myös.

Se, mikä on outoa, on että fysiikassa tarvitaan tän tyylisiä otuksia. Voiko elektroni muka olla vyö jonka toinen pää on naulattu kiinni avaruusaikaan? Yks idea mikä mulle tuli mieleen on, että elektroni on orientoitunu säite, jonka toinen pää on naulattu kiinni braaniin.  Mutta tässä ei varmaan oo järkee, ja jos on, löytyy säieteoriasta varmasti pohdintaa asiasta. Tää kommentti kannattaa ottaa lähinnä läpällä.

Yks hankaluus mikä tässä tulee on, että ortonormaalia kantaa ei voi yleisessä tapauksessa valita koko avaruuteen. Taitaa olla karvapallolause joka sen estää. On siis pakko peittää avaruus atlaksella ja valita monta eri  ortonormaalien kantojen kenttää. Siinä käy sitten niin, että joskus näitä alueita on päällekkäin 3 kpll. Tässä tapauksessa meillä on muunokset, jotka liittää nää alueet toisiinsa. jos hyvin käy, niin kun mennään syklisesti alueesta toiseen ja tullaan takasin, ei oo tapahtunu mitään. Mutta koska nää muunnokset on unversal covering groupin alkioita, voi käydä niin, että ei tullakaan takasin, vaan on jääny jäljelle polkuavaruuden homotopiaryhmän verran muunnosta, eli käytännössä kertominen miinus ykkösellä. Tää on ongelmallista teorian konsistenssin kannalta.

Joskus voidaan noi patchit ja muunnokset niiden välillä valita siten, että noita miinus ykkösiä ei tule. Tämmöstä monistoa sanotaan spin-monistoksi. Muistaakseni orientoituvuus ei riitä siihen että monista on spin-moonisto; kyse on siis todellakin epätriviaalista asiasta. Ja noita valintoja voidaan joskus mös tehdä useammalla tavalla. Tällön monistolla on useampi niinsanottu spinrakenne.

Tää on (ainakin mun nähdäkseni) ongelmallista fysiikan kannalta. Kuinka on mahdollista, että spinorien olemassaolo (eli käytännössä elektronin olemassaolo) asettaa rajotteita moniston globaalille topologialle? Tä\tä mä en voi käsittää. Luulis että jos spinorikenttä (siis esim. elektroinikenttä) voi olla olemassa lokaalistti, voi se olla olemassa myös globaalisti, olipa tavaruusajan topologia mitä hyvänsä (Minkowski, madonreikä, musta atukko etc.).

Sitten vielä huomio siitä, että tarviiko spinori ortonormaalin kannan. NLukeman perusteella näyttäs siltä, että tarvii. Mutta mä en oo kuitenkaan varma tästä. Miksei me voitas alottaa GL(n):n polkuavaruudesta ja tehdä homotopiaekvivalenssi sinne? Luulis että thomotopialuokiks tulee samat kun SO(n):n tapauksessa, kun GL(n) on kuitenkin kontraktoitavissa (vai mikä se sana on) SO(n):ks. En tiiä meneeks tässä joku pieleen, vai oisko se syntyvä ryhmä jotain niin esoteerista ettei sitä kukaan haluu käsitellä.

En tiiä onko tolla vikalla pointilla kauheesti väliä. Mun mielestä tärkeintä ois ymmärtää, että mitä spinorit konkreettisesti vois olla ilman koordinaatistoja ja maata, johon vyön voi hakata kiinni, ja ymmärtää, että minkä takia spinorin olemassaolo vaatii epätriviaaleja ominaisuuksia moniston topologialta.

---

*2017-11-04 14:10:28.431501*

# Muistivihko on loistava keksintö

Tää muistivihko on ratkassu sellasen ongelman, että kuinka ottaa päässä pyöriviä ideoita talteen. Kaikki mitä tässä on tällä htekellä on syntyny parin vuorokauden sisällä, eikä sen kirjottamien oo vieny kuin pienen murto-osan ajasta ja energiasta tuona kahtena vuorokautena.

Mä oon hämmästynyt siitä, kuinka paljon se, että editointi ei oo mahollista, helpottaa kirjottamista. Se poistaa sen neuroottisen halun hinkata tekstistä täydellistä, mikä yleensä tai ainakn todella usein mun kohalla johtaa siihen että mitään ei saa kirjotettua.  Mit\ mä lukasin noita tekstejä tossa yläpuolella, niin tällä lailla tulee jopa ihan ok"ta tekstiä. Jos täst\	q alusta voi yhtään päätellä mitään, niin tulevaisuudessa mä saan ajatuksia talkteen aivan perkeleesti enemmän ku aikasemmin.

Tää on sinän\sq jännä, että mä oon aina kelannu, että käyttäjä pystyy kyllä pistämään teknologian houkutuksille vastaan jos se sitä haluaa. Mutta mä en oo jky\enny aikasemmin pistää editointihalulle vastaan. Tai ehkä mä en vaan oo älynny kunnolla haluta sitä. No, aivan sama, tää\ nyt on joka tapauksessa ratkassu sen ongelman.

By the way, mä kirjotan tä\tä sokkona. En tiiä kuinka pajon tulee typoja. Mun 10-sormijärjestelmä on aika paska (ei se edes ole mikään kymmensormijärjestelmä), ja sormet varmaan kyllä väkillä osuu mihin sattuu. Saa nähä miltä tää näyttää .

---

*2017-11-04 14:36:53.936531*

# Soittimen soittamisen opettelu

Soittamista opetetaan monessa paikassa nykyään. Vallitseva opetus- ja opettelutyyli (opettajan antamia harjotuksia yms.) on mun mielestä aivan perseeä. Mä kerron miten mun mielestä kannattaa opetella soittamaan. Käytän esimerkkinä kitaraa, koska ite oon opetellu soittamaan sitä, mutta resepti on kyllä aivan yleisesti pätevä.

Soittaminen koostuu kahdesta asiasta: päässä on jotain, mitä haluaa soittaa, ja selkärangassa on taito jolla sen asian saa sitten soitettua.

Ensimmäistä näistä ei rehellisesti sanottuna voi oikeen harjotella. Ideoita saa kuuntelemalla musaa, ja jokainen kuuntelee musaa niin paljon kun haluaa. Musan kuuntelusta ja fiilistelystä ei tuu yhtään mitään jos sitä ei halua tehdä. Tiedän, moni tekee kyllä sitä tietosesti ja analyyttisesti, mutta se on aivan turhaa paskaa jos mutla kysytään. Musiikki on tunteiden ilmasua, ja ilmasemisessa ei oo kauheesti pointtia jos ei oo tunteita mitä ilmasta.

Toinen puoli, eli se miten niitä ääniä mitä päässä on saa pihalle, on sitten harjoteltavissa oleva juttuu. Soitonopettaja antais skaaloja ja sointukarttoja joita lueskella ja opetella ulkoa. Se on perseestä. Järkevä tapa on käyttää tieteellistä menetelmä
ä, eli ottaa soitin käteen ja kokeilla että mitnkä kuulosia ääniä tulee mitäkin tekemällä.

Eli siis: ota kitara kouraan ja paina joku kieli kiinni otelautaan ja näppää kieltä. Siitä tulee joku ääni. Sitten ota toinen kieli ja toinen kohta ja kuuntele kuinka erilainen ääni siitä tulee. Tätä kun tekee tarpeeks, alkaa tietää selkärangan kautta että mihin pitää laittaa sormet ettää saadakseen soitettuaan sen melodian mitä päässään kuulee.

Sointujen opettelu menee samalla tavalla: soittaa yhtä aikaa monta ääntä ja kuulostelee että miten ne sointuu yhteen. Sitten alkaa pikkuhiljaa osata soinnuttaa biisejä.

Tällälailla oppii kaikki soinnut ja asteikot mitä opettaja sulle tyrkyttäs (paitsi tietysti niitä ei opi mitkä ei kiinnosta, mutta niitä ny ei tarviikkaan oppia).

Tietysti neuvoa voi kysyä guruilta. Järkevä kysymys on tyyliin “Miten sä soitat tän jutun?” eikä “Miten mun pitäs soittaa tää juttu?” Siinä on vitun moinen ero. Ideoiden ammentaminen gurulta on viisasta, mutta gurun totteleminen taas tyhmää. Ja siis onhan musiikin kuuntelukin tämmöstä guruilta ideoiden ammentamista.

Pikku hiljaa sitten se, kuinka kitaraa soittaa, ja kuinka siitä saa ääniä, alkaa sitten toimia myös ideoiden lähteenä. Se, miltä kitara kuulostaa, ei todellakaan ole pelkästään sitä, millanen vahvistin on ja millaset kielet yms. Kitaralla on luonnollista soittaa tietynlaisia juttuja, ja ne kontribuoi siihen mikä me tunnetaan saundina nimeltä kitara.

Tää voi tietysti kuulostaa saakelin hankalalta ohjeelta. Mitä jos mulla ei oo päässä mitään mitä soittaa? Mitä jos mä en opi soittamaan sitä mitä mä haluan soittaa vaikak kuinka otellautaa hipelöin ja kokeilen eri juttuja? Vastaus tähän on, että sillon sun ei pidä soittaa kitaraa.

En mä tarkota että s
ä olisit jotenkin liian huono kitaransoittajaks. Vaan tarkotan sitä, että selvästi sä et halua soittaa kitaraa. Sä et fiilistele sitä, ja sillon siihen ei kannata uhrata aikaa. Mäkään en enää nykyään soita kitaraa, paitsi joskus ihan vähän, koska mä oon vaan kadottanu sen fiiliksen. Muut jutut on tullu sen tilalle (esim. tää kirjottaminen).

Mä yritin soittaa saksofonia viime keväänä, mutta tajusin että en oikeesti halunnu soittaa sitä. Vähän aikaa opettelin sitä tähän tyyliin niinkun tässä kerron, ja opin jotain. Mutta sitten tajusin, että mun saksofoninsoittofiilis oli oikeesti hyvin laimee, ja halusin oppia soittamaan siks, että ajattelin, että saksofonistit on cooleja tyyppejä. Tajusin kuinka hölmö olin ollu ja jätin saksofonin sikseen.

Ei oo todellakaan itsestäänselvän helppoa tietää että mitä haluaa. Maailma antaa niin perkeleenmonta kadehdittavaa roolimallia että niitä alkaa matkimaan ihan huomaamattaan. Omia fiiliksiään pitää vaan opetella kuunteleen. Se on saakelin hankalaa. Jos nyt jonkun konkreettisen vinkin tähän laittas, niin se on, että aina kannattaa kyseenalaistaa ja miettiä että miks vitussa.

---

*2017-11-04 15:11:07.071120*

# Selitin kaverille kuinka hyvä idea tää muistivihko on.

![audio/2017-11-04-15-11-06-946212.ogv](audio/2017-11-04-15-11-06-946212.ogv)

---

*2017-11-04 15:37:38.985506*

# Äänimuistiinpanoista

![audio/2017-11-04-15-37-38-866220.ogv](audio/2017-11-04-15-37-38-866220.ogv)

---

*2017-11-04 18:34:37.833376*

# Muistivihon tekninen toteutus

Kun mä kirjotan tähän muistikirjaan, mä ajan komentorivillä ohjelman [typewriter.py](typewriter.py). Se avaa promptin, johon mä kirjotan rivi/kappale kerrallaan tekstiä. Sitten, kun postaus on valmis, kirjotan promptiin komennon `\quit` ja postaus menee suoraan internettiin . Mä voin tehdä tän läppärillä tai sitten puhelimellaaa, kiitos Termux-äpin. Puhelimella käytän joko onscreen-näppistä tai bluetoothnäppistä. Käytännössä tää on siis kirjotuskoneen nykyaikaistettu versio.

TTeknisesti homma perustuu Git-repositoryyn, jossa on yks tiedosto, `README.md`. `typewriter.py` appendaa rivejä sen perään. Kun kirjotus on valmis, committaa skripti muutokset automaattisesti repositoryyn ja pushaa sen GitLabiin, josta sitä voi heti tutkailla.

Tää ei ole varmaankaan teknisesti mitenkään elegantti ratkasu. Mä tiedän koodaamisesta hyvin vähän ja oon väsänny tän systeemin sillä periaatteella että kuhan toimii. Mutta. Tää *on* käsittämättömän elegantti ratkasu siinä mielessä, että tää tekee ajatusten talteenlaittamisesta erittäin helppoa ja vaivatonta sekä teknisesti että psyykkisesti. Siinä mielessä tää on eleganttia teknologiaa. Ja loppujen lopuks ainoastaan sillä on väliä.

Muutamia ongelmia on. Ensinnäkin typoja ei huomaa kovin helposti tohon promptiin kirjottaessa. Olis hyvä jos siihen sais jonkun systeemin mikä näyttäis jos jotain on pielessä, varsinkin kun kirjottaa jotain monimutkasempaa (linkkejä, koodia, matematiikkaa) joissa syntaksilla on merkitystä. Tässäkin muistiinpanossa on jolinkkejä ja koodia, enkä mä tiedä tätä kirjottaessani että onko ne oikein.

Ainiin, jos joku testaa tota skriptiä, tai skriptiä [audio.py][audio.py], kerrottakoon että ne tekee automaattisesti `git pull`, committaa muutoksia ja sit tekee `git push`. En tiiä onko toi jonkun hyvän tavan vastasta mutta mun käytössä vähentää manuaalisten steppien m\qärää.

GitLab rendaa nää muistiinpanot vähän karulla tavalla, ja sivulla on kaikkee ylimäärästä paskaa, ja tietysti näitä typoja on saatanasti. Se on hyvä asia. Ne kaikki poistaa sellasta vaikutelmaa että tää yrittäis olla jotenkin valmista tai viimeisteltyä settiä, ja se taas vä\hentää mun paineita kirjottamisessa. Mulla ei oo oikeestaan minkäänlaisia paineita, mikä heijastunee siihen että teksti soljuu mukavasti.

Tähän väliin konkreettinen esimerkki siitä, mitä tää kirjotuskonemaisuus tekee tälle kirjottamiselle. Mä kirjotin edellisen kappaleen loppuun “teksti soljuu mukavasti.” Mä en voi muuttaa sitä enää (tai voisin, mutta pitäs säätää, mutta se ois liian vaivalloista ja vastoin uskomusta jonka oon tässä itelleni kehittäny (sen mukaan ei saa muuttaa jo kirjotettuja kappaleita)), joten mulle ei tuu mieleen kelata että oliko se nyt hyvä ilmaisu vai ei. Yleensä ne ilmasut mitä ekana tulee mieleen on ihan hyviä. Se, että jäis miettiin ilmasua, tuhois flowta. Editointimahollisuuden puuttumisella on suuri psykologinen vaikutus. Ehkä mä opin joku päivä kirjottaa perus vimillä ilman että jään editoimaan neuroottisesti tekstiä, mut tässä vaiheessa tää on todella hyvä ratkasu.

Tossa GitLabiin ilmestyvässä sivussa on pari teknistä ongelmaa. Ensinnäkin se ei oo sellanen perus staattinen nettisivu, vaan se generoidaan ilmeisesti selaimessa jotenkin. En tiiä teknisesti miten se menee. Mut jokatapauksessa se vois olla yksinkrtetasempi. Google Translate ei osaa kääntää sitä, mikä tarkottaa sitä, että en voi jollekin suomea ymmärtämättömälle tyypille linkata helposti käännöstä muistiinpanosta.

Toinen ongelma on, että jos mä joskus esim. nimeen  ton markdown-tiedoston uudelleen joksku muuks ku `README.md`:ksi, silloin vanha muistiinpanoon laitettu linkki lakkaa toimimasta. Ehkä mun pitää vaan pitää kaikki muistiinpanot tässä `README.md` tiedostossa. Jos siitä tulee kauheen pitkä, on vaarana että sen rendautuminen  selaimessa kestää kauan. Saa nähä miten käy.

Ja tästä voi kyllä tulla nopeesti aivan saakelin pitkä. Äsken ku katoin oli word count 4457, mikä tekee  kai jotain 15 kirjan sivua, ja oon vasta ihan päässy alkuun. Perkele sentään, tää on niin VITUN hyvä keksintö.

Sitten tässä on vielä toi äänimuistiinpanon laittamismahdollisuus. GitLab flavored markdown tukee videoiden  näyttämistä. Se toimii `.opus` äänitiedostoilla kun laittaa vaan tiedostopäätteeks `.ogv` (tai puhelimen chrome ei soita jostain syystä pitkiä äänimustiinpanoja, mutta läppärillä firefoxilla toimii ok:sti). Toi sskripti [audio.py][audio.py] ottaa kansiosta `../downloads` kaikki `.opus` tiedostot ja tekee niistä äänimuistiinpanon muistivihon jatkeeks.

Käytännössä mä käytän sitä silleen että lähetän whatsapissa ääniviestin jollekin tyypille ja selitän jonkun jutun. Sitten jaan sen Termuxiin ja ajan `.python audio.py` (pitää olla tässä repon rootissa). Jos en haluu selittää kellekään tyypille, lähetän ääniviestin sellaseen group chattiin joka on olemassa vaan tä\t juttua varten. Haluun äänittää whatsapilla koska oon tottunu siihen ja se toimii näppärästi ja robustisti.

Toi äänimuistiinpanon lisääminen on hyvä niitä tilanteita varten että on pihalla liikkeessä, tai jos tulee joku saakelinmoinen innostus että pitää päästä vaahtoomaan. Tai jälkimmäinen on oletus, en oo testannu sitä vielä.

Yhteenvetona sanoisin, että saatana, tää on teknologian käyttämistä parhaimmillaan. Teknisistä yksityiskojhdista viis, tärkeintä on että joku todellinen ongelma ratkee ilman, että joutuu käyttää liikaa vaivaa. Täydellisyyteen pyrkiminen ei oo mitään muuta ku destruktiivista. Ihmisten kannattas osata koodata ja ymmärtää tietokoneita jotta vois tehä tällasia juttuja itelleen (ja muille, jos jotakuta kiinnostaa.)

---

*2017-11-04 23:56:42.958607*

# KMiks ääkköset menee välillä kummasti

Kaveri kysy multa että miks mulla on ääkkösiä varten tollaset erikikoismerkkikombinaatiot. Esim. jos kirjotan `\q` muuttuu se ä:ks.

Vastaus on, että tässä komentorivvisysteemissä mikä mulla on puhelimessa (äppi nimeltä Termux, ihan vitun hyvä btw) mä en osaa vaihtaa bluetoothnäppiksen leiskaa pois uUSaAsta. Joten päätin tehä koodiin tollaset jutut että pystyy kirjottaa suomee usa leiskalla. Vähän purkka ratkasu, mutta toimii. Ja se on tärkeintä. Huono puoli on että aina sillon tällön tulee typotettuu, ja vaikka sanan “mäti” sijaan tulee “mq\ti”.

---

*2017-11-05 00:18:05.027984*

# Kenelle mä kirjotan?

Mä kirjotin aikasemmin että mä kirjotan **itelleni**, mutta se ei ooo kyllä aivan totta, ainakaan joka kerta. Joskus mä kirjotan itulevaisuuden versiolle itestäni asioita muistiin ja joskus mä kirjotan joollekkin tietylle ihmiselle tai ihmisille. EEsimerkiks spinorikirjotus oli puhtaasti semmonen juttu.

Käytännössä mä kirjotan näissä molemmissa tilanteiss ainakin jossain mielessä ideaaliselle kuuntelijalle, ikäänkuin mielikuvitusystävälle. Itelleen nyt ei voi oikeasti kirjottaa (mä tiedän kaiken mitä meinaan jo kirjottaa, joten mikä ois pointti?) ja jos kirjottaa jollekin muulle oikeelle ihmiselle, ei voi tietää millanen tyyppi se on. Se kuulija on pakko ikäänkuin keksiä ite.

Se, kuinka hyvää ja toimivaa tekstiä syntyy, riippuu aika pitkälti siitä, kuinka nappiin osaa kuulijan mielessään kuvitella, siis kuinka hyvin pystyy samastumaan kuulijaan. Tääon menny mulla usein aivan päin vittua, eikä sitä voi oppia kirjottamalla. Pitää olla vuorovaikutuksessa ihmisten kanssa ja aktiivisesti koittaa kattoo juttuja toisen perspektiivistä.

Tää ei oo mitään muuta kuin ihmiselämän ikuinen dilemma: kuinka ymmärtää toisia ihmisiä, ja kuinka tullaymmärretyks. Jäkimmäinen vaatii ensimmäisen onnistuakseen (tai sitten kuulijan pitää olla superihmistuntija). Helppoo tietä ei oo asian kiertämiseks, ja työtä vaan pitää tehä. Yritys, erehdys, yritys, erehdys…ja ehkä joku päivä onnistuminen.

Yks universaali sääntö tässä hommassa tuntuu olevan. Kirjottipa sitä kenelle tahansa, pitää pystyä kirjottamaan sellasella fiiliksellä, että lukija on halukas kuulemaan ja ymmärtämään. Siis että kirjottaja ja lukija on samalla puolella, tekemäasä yhteistyötä. Jos tähän ei pysty, tulee tekstiin heti epävarmuuden fiilis. Puolustuskannta vaan paistaa läpi, ja siitä tulee tpperseestä revitty fiilis lukijalle. Kun mä luen jotain omaavanhaa tekstiä missä on ollu puolustuskannalla kirjottaess, meinaa laatta lentää.

Ton yhteistyöfiiliksen saamiseks on tärkeetä että ei liikaa tuputa tekstejä muille. Kun oon kirjottanu facebookkiin, oon toivonu jotain tietynlaista reaktiota, ja pettyyny ainakin miljoona kertaa. Siinä tukee semmonen olo että mun yleisö on mua vastaan, ja se näkyy seuraaavissa teksteissä. Mä aion pitää nää tekstit aika pitkälti omana tietonani, vaikka ei se tietysti haittaa jos muut lukee. Pointti on että mä en mieti sitä että lukeekojoku muu näitä juttuja.

Mikä pointti sitten koko kirjottamisessa on? No se on, että tää on *sketsailua*. Mä laitan ajatuksia talteen, jotta voin myöhemmim koostaa niistä jotain viimeisteltyä. Sitten mä kyllä teen hommaa sillä  ajatuksella että muut lukee ja elaiitan itteni samalla alttiiks masennukselle. Mutta puolustuskantafiiliksen ja epävarmuuden todennäköisyys on sillon pienempi, koska mä olen sketsaillu ne jutut etukäteen ilmman paineita, täydellisen ymmärtäväiselle mielikuvituslukijalle.

---

*2017-11-05 00:50:34.810196*

# Yhdyssanat

Suomenkielessä on paljon yhdyssanoja jotka pitäis kiieliopin mukaan kirjottaa yhteen. Mä olen sitä mieltä että njoskus väli kannattaa kuitenkin laittaa vaikka säännöt sen kieltäiskin.

Tää johtuu siitä, että uhdyss
sanojen oikeinkirjotus saa aikaan tietynlaisen kirjakielisen fiiliksen aikaan. Jos kirjottaa l“linja auto/" sen sijaan että kirjottas “linja-auto”, tulee rennompi fiilis. Se on ikäänkuin murretta.

Moni on eri mieltä jyrkästi. Ja moni varmaan kelaa et mä en ns. osaa yhdyssanoja. Vittu miten perseestä! Se on vähän sama ku valittas maalarille että sen maalaama taivas ei oo joka kohdasta saman värinen.

Tietysti voi olla että muille ei tuu rentoo fiilistä yhdyssana virheistä. (Oisin muuten voinu typottaa tän muistiinpanon otsikon, mut ei jaksanu ku se ois ollu aika klisee.) Mä assosioin yhdyssanavirheen rentouteen varmaan siks, että oln nähny rentojen ja hyvien tyyppien kirjottavan yhdyssanoja pöin vittua. En tiiä miks ne on tehny niin—ehkä ne kelaa niinku mä, tai sittten ne rentous ja hyvätyyppiys korreloi yhdyssanojen osaamattomuuden kanssa. Em ihmettelis; tekninen taitavuus kun korreloi yleensä mielikuvituksettomuuden ja tiukkapiposuufen kanssa.

---

*2017-11-05 02:22:33.275373*

# Syntaktisia typoja poistava ominaisuus

Typot haittaa erityisen paljon linkkien, koodiblokkien, matematiikan yms. kirjottamista, koska ne on tarkkoja syntaktista. Lisäsin typewriteriin sellasen ominaisuuden, että muistiinpanoon voi insertoida pätkän vimillä kirjotettua tekstiä, ja sillä lailla että syntax highlight toimii sillä kielellä millä haluaa. Laitoin homman sillälailla että tyhjät rivit vimillä editoiduista pätkistä poistuu, joten editointia voi harrastaa maksimissaan yhen kappaleen alueella. Tällä lailla kirjotuskonemainen olemus säilyy eikä neuroottiseen koko tekstin editoimiseen synny mahdollisuutta.

## Kokeillaan toimiiko homma

Ensin vois kokeilla ihan perus markdown-listaa ja laittaa siihen vaikka pari linkki
ä.

* [stallman.org](http://stallman.org)
* [konstakurki.org](http://konstakurki.org)
* [wikipedia.org](http://wikipedia.org)
* [*Wikipedia*: TL;DR](https://en.wikipedia.org/wiki/TL;DR)

Noniin, olipa helppo kirjottaa. Luulisin että meni oikein.

Sitte vähä matematiikkaa.

```math
D_\alpha V_\beta = \{{_\alpha}{^\gamma}{_\beta}\}V_\gamma + \partial_\alpha V_\beta
```

Nyt pitäs olla kovariantti derivaatta oikein (aikasemmin yritin ja meni pieleen).

Sitte tauluko.

| Kylä | Asukasluku |
| :---: | :---: |
| Tikkala | 500 |
| Moksi | 10000 |

Python-koodia.

```python
while True:
    print('miks mulle ei tuu muuta mieleen ku infinite loop')
def funktio():
    print('No okei kyl sitä sitte tulee ku oikeen rassaa nuppia')
```

Ja vielä $`\KaTeX`$ia (koodina).

```tex
\begin{equation}
    f(x) = 2x^2
\end{equation}
```

Ja niin joo kyllähä HTML:ää pitää aina testaa.

```html
<ul>
    <li>Jos vaikka t\qllasta listaa v\qs\qis</li>
    <li><a href='http://konstakurki.org'>Mun nettisivu</a></li>
</ul>
```

No niin, katotaanpa miten onnistu.

---

*2017-11-05 02:37:21.033667*

# Testataan uus ominaisuus puhelimella

Veikkaan, että tässä tuskin menee mikään pieleen, mutta kokeillaan ny kumminkin että toimiiko ttää myös puhelimella.

```python
print('python-koodiblokki')
x = 'laitetaa ny viela toinen rivi')

---

*2017-11-05 13:40:42.058110*

# Erikoismerkkien syöttöä fiksummaks

Kaveri jeesas mua ja sain `altgr-intl`-näppäimistölayoutin toimimaan puhelimessa. Siitä saa vaikka mitä erikoissymboleja, muun muassa ääkköset ja symbolit ‘’“”. m-dashia, n-dashia ja kolmee pistettä en löytäny siitä, joten niille jätin erikoimerkkiyhdeistelmät. n-dash (–) tulee kun kirjottaa `\n-`, kolme pistettä (…) tulee kun kirjottaa `\...` ja m-dash (—) tulee kun kirjottaa
`¬`.
Toivottavasti meni oikein. Tässä kirjotyuksessa ei oikeestaan ollu muuta pointtia ku kokeilla että toimiiko noi jutut.

---

*2017-11-05 13:49:42.599366*

# Voi perkele

```

## Voi perkele, unohtu toisiks edellisessä muistiinpanossa lopettaa koodiblokki. Se voi kyllä tapahtuu helposti ja sit kaikki menee aivan päin vittua, pitääpä virittää joku automaattinen systeemi sitä varten.

Kokeillaas ny vielä noi uudet erikoismerkkisymbolijutut. äöäÄÖÅ“”‘’ tulee suoraan `altgr-intl`-näppäimistöleiskasta. Erikois sequenset on merkkejä — (m-dash), – (n-dash) ja … (kolme pistettä) varten. Ne sequencet on
`¬`, `\n-` ja `\...`.

Josko ny toimis ja koodiblokki ois loppunu.

---

*2017-11-05 14:28:13.084145*

# Testaammisen vaikeus

Nykymaailman ongelma on siinä, että eläminen vaatii sellaisten teorioiden—joita tässä tapauksessa kannattaisi kutsua uskomuksiksi—noudattamista, joiden testaaminen on käytännössä mahdotonta. Pitää uskoa lakiin, rahaan, lääketieteeseen.

Tämä ei ole meille millään tasolla luonnollista. Heimoyhteiskunnassa kaikkea tietämystä testattiin kokoajan. Nuotio pitää saada syttymään, onnistuuko se tällä tavalla? Hetkinen vain ja asia on kokeiltu. Tuolta löytyy hyviä hedelmiä, vai löytyykö? Sekin selviää nopeasti kun käydään katsomassa.

Ainoat asiat, mihin heimoaikoihinkin on pitänyt uskoa sokeasti, on myrkylliset ruoat. Sen takia meillä on iljetys-tunne (se viestiii muille, että tätä ei kannata syödä), ja ruokatabut, jotka mahdollistavat heimon kollektiivisen kyvyn välttää vaarallista syötävää.

Uskonnot ovat hyvä esimerkki siitä, kuinka pahasti arviointikykymme voi heittää, kun toimimme maailmassa, jossa ei-testattavissa-olevat teoriat kukoistavat. Veren määrä joka taivaspaikan toivossa on vuodatettu on käsittämätön.

Nykyään olemme saattaneet hiukan viisastua ristiretkien ajoista—mutta emme kuitenkaan tarpeeksi. Ei-testattavissa-oleva uskomme talouskasvuun, immateriaalioikeuksiin ja rahaan ohjaa meitä tuhoamaan elinympäristömme ja ehkä jopa itsemme.

---

*2017-11-05 17:16:31.359344*

# Aina meinaa tulla hinkattua liikaa

Tää muistivihko toimii hyvin. Kuitenkin mä meinasin alkaa opettelemaan sivugeneraattorin käyttöä ja tehdä tästä ns. oikeen nettisivun. Toki sen vois tehdä, eikä siihen varmaan mahottoman kauaa menis, mutta mä silti veikkaan, ettei mun kannata sitä tehdä.

Ensinnäkin, tällä muistiviholla ei ole lukijoita, enkä mä siihen edes pyri. On aikamoista energian haaskausta hinkata julkisivua yleisölle, jota ei ole olemassa. Toisekseen, jos tästä tekis “oikean” nettisivun, lois se heti sellasen fiiliksen, että tän setin pitäs olla jollain tavalla viimeisteltyä, joka taas lois paineita kirjottaa valmista ja laadukasta tekstiä.

Lisäksi sivun hinkkaaminen veis huomion pois kirjottamisesta. Ajatuksia jäis talteen vähemmän.

Ei perkule, pitää mennä.

---

*2017-11-05 17:45:49.745986*

# Jatkoa edelliseen

Niin, sivun hinkkaaminen vie huomion pois kirjoittamisesta, joka johtaa siihen että muistivihon varsinainen käyttötqrkotus, eli ajausten talteenpoimiminen, ei toteudu täysillä.

Lisäks on semmonen juttu, että mä olen nyt päässyt vauhtiin tän muistion kanssa, ja sivugeneraattorin käyttäminen yms. saattas vaatia muutoksia tähän paradigmaan. Se ois huono juttu—jos tämmöseen on saanu vauhtia, on syytä käyttää se momentum mikä siihen on latautunu.

Tää on instanssi yleisemmästä totuudesta mikä näkyy nykymaailmassa joka paikassa . On helppo suunnitella, rakentaa, ratkaista ongelmia, jotka on kiehtovia ja juuri sopivan haastavia. Mutta kiehtovuus ja sopiva vaikeusaste ei ole sama asia kuin merkittävyys. Maailmassa ratkotaan vaikka kuinka paljon turhia ongelmia ja jätetään merkittäviä ongelmia aivan ilman huomiota.

---

*2017-11-05 18:26:38.261391*

# Mistä raha tulee?

Raha on maailman universaalein ja pinttynein uskonto. Mistä se tulee? Enkä nyt tarkota, että miten nykyaikainen raha, kuten € tai $ tai ¥ painetaan, vaan sitä, mistä koko rahan konsepti tulee. Ymmäëtääksemme sen täytyy meidän miettiä asioita useamman tuhannen vuoden takaisesta perspektiivistä.

Olipa yhteiskunnassa rakennetta tai ei, on omistusoikeuden konsepti olemassa. Se on luonnollinen reviirin jatke niihin asioihin, joita ihminen kantaa mukanaan. Poimin hedelmän maasta, joten se on minun. Älä tule repimään sitä kädestäni.

Kun omistusoikeus on olemassa käsitteenä syvällä ihmisten selkärangassa, on aika suoraviivaista päätyä tekemään sopimuksia, joissa tavaran omistusoikeus siirtyy henkilöltä toiselle. Jos annat mulle keihään, saat säkillisen omenoita.

Tällainen vaihtokauppa mahdollistaa sen, että kaikkien ihmisten ei tarvitse tehdä kaikkea, ja että yksittäiset ihmiset voivat perehtyä johonkin tiettyyn asiaan erityisen syvällisesti. Se mahdollistaa yhteiskunnan jäsenten erikoistumisen erilaisille aloille.

Pelkkä vaihtokauppa on kuitenkin epäkäytännöllistä. Mitä jos mä haluan ostaa sulta kengät, mutta mulla ei satu olemaan mitään, mitä sä juuri nyt sattuisit tarvitsemaan? Voin yrittää käydä ensin kauppaa jonkun muun kanssa, jotta saisin jotain mitä vaihtaa tekemiisi kenkiin, mutta hankalaksi se jokatapauksessa menee.

Toiset hyödykkeet soveltuvat paremmin tuollaisessa tilanteessa tarvittavaksi vaihdon välineeksi. Helppo mitattavuus ja kuljetettavuus ja hyvä säilyvyys ovat sellaisia ominaisuuksia, jotka edesauttavat hyödykkeen muuttumista vaihdon välineeksi.

Sä voisit esimerkiksi suostua ottamaan multa vastaan säkillisen suolaa, koska sä saattasit ajatella, että se on helppo myydä eteenpäin. Ehkä myös simpukan kuoret kävis, koska jotkut kuulemma pitää niitä kauniina. Tai oravannahat, tai kulta.

Näistä vaihdon välineeksi kelpaavista hyödykkeistä joku alkaa pikkuhiljaa erottua muista. Jos käy vaikkapa niin, että ihmiset sattuvat käyttämään useammin kultaa kuin muita hyödykkeitä, nousee ihmisten tapa arvostaa kultaa, koska kaikki ajattelevat että kulta nyt ainakin kelpaa vaihdossa. Tällä lailla kulta muuttuu pikkuhiljaa yleispäteväksi valuutaksi ja muiden hyödykkeiden, jotka vielä jonkin aikaa sitten olivat ehdolla samaan asemaan, arvoa aletaan mittaamaan suhteessa kultaan.

Tällä tavoin kulta on muuttunut rahaksi. Kullan arvo ei perustu sen hyödyllisyyteen, vaan siihen, että kaikki uskovat sen arvoon. Tämä uskon on aivan omaa luokkaansa ihmiskunnassa vallitsevien uskomusten joukossa: ihminen uskoo kullan arvoon, koska se uskoo muiden uskovan sen arvoon. Ja tällaisessa maailmantilanteessa tuo usko on aivan paikkansa pitävä asia. Kun kaikki ihmiset ajattelevat tällä tavoin, on kullasta tullut kaikista ihmismaailmaa ohjaavista voimista suurin.

Tämä on kertakaikkiaan eriskummallinen juttu. Toisin kuin vaikka usko kuolemanjälkeiseen elämään, on usko rahan arvoon yksilön näkökulmasta täysin rationaalista toimintaa. Mutta ihmisten muodostaman kokonaisuuden kannalta se on monessa mielessä tuhoisaa. Järjestäytynyt rikollisuus, sodat ja poliittinen korruptio pyörivät kaikki rahan ympärillä.

Raha ei ole järjestelmä, ainakaan siinä mielessä, että joku olisi sen järjestänyt toimimaan. Raha on täysin spontaanisti yhteiskunnassa esiintyvä ilmiö, jota ei voi päästä pakoon oikein mitenkään. Ihmisten kokemus reviiristä ja kahdenkeskisten sopimusten pitävyydestä riittää rahan syntymiseen.

Tietysti hallitsevan valuutan rinnalla on olemassa muitakin valuuttoja. Vuonna 2009 syntyi maailmaan bitcoin-niminen ns. kryptovaluutta. Se perustuu salausmatematiikkaan, ja Bitcoinin omistaminen tarkoittaa yksinkertaisesti sitä, että tietää tietyn numeron, jota muut eivät tiedä.

Tarinan (saattaa olla tosi) mukaan joku osti joskus pizzan kymmenellä miljoonalla Bitcoinilla. Nykyään yhdellä bitcoinilla saisi tuhat pizzaa. Ihmisten usko Bitcoiniin on siis kasvanut huimasti. Miksi? Osittain siksi, että Bitcoin on monessa mielessä parempi valuutta kuin vaikkapa US$. Loput selittyy sillä, että ihmiset ovat uskoneet, että muiden ihmisten arvostus Bitcoinia kohtaan kasva tulevaisuudessa.

Kuulostaa pöljältä, mutta näin se vain menee. Valuuttojen kurssit muuttuvat sen mukaan, kuinka ihmiset arvioivat toistensa arvostusta ja sen muuttumista tulevaisuudessa. Kun kaikki tekevät samaa, muuttuu arvon käytös hyvin kaoottiseksi. Kurssit pomppivat tavalla, jota kukaan ei oikeasti voi kunnolla tajuta.

En ole vielä sanonut mitään pankeista, joden rooli nykyaikaisissa virallisissa valuutoissa (€, $, ¥,  ,…) on merkittävä. Pankit astuvat kuvaan siinä vaiheessa, kun ihmiset alkavat kokea esimerkiksi kullan liian epäkäytännöllisenä vaihdon välineenä.

Kullan säilyttäminen kotona on riskialtista, sillä varas voi tulla ovesta sisään. Kullan mukana kanniskelu on ikävää, koska kulta painaa paljon. Kullan käyttäminen maksamisessa silloin, kun kaupankävijät eivät ole samassa paikassa, on epäkäytännöllistä, koska kullan kuljetus pitää järjestää jollain tavalla.

Pankit ratkaisevat tämän ongelman tarjoamalla kullansäilytyspalvelua. Sä viet kultasi pankin holviin, jossa pankin vartijat pitävät siitä huolen. Pankin asiakkaat voivat maksaa toisilleen kultaa näppärästi vain ilmoittamalla pankille, että tämän tyypin kullasta on nyt siirrettävä tämän verran tuolle tyypille.

Tässä vaiheessa pankit eivät ole muuttaneet hommaa vielä mitenkään merkittävästi. Mutta pankkiiri äkkiä tajuaa, että tällä on tolkuttomat läjät kultaa holvissaan makaamassa tyhjänpanttina. Vain harvoin asiakkaat tulevat hakemaan sitä pois. Joten pankkiiri ajattelee, että ehkä sitä kultaa voisi käyttää jotenkin.

Niinpä pankkiiri alkaa tehdä asiakkaiden kanssa sopimuksia, joiden mukaan pankkiiri voi käyttää asiakkaan kultaa haluamallaan tavalla, kunhan asiakas saa pienen koron vastineeksi, ja lupauksen siitä, että pankkiiri kaivaa jostakin kultaa asiakkaalle sitten kun tämä kultansa haluaa takaisin.

Tässä vaiheessa on tapahtunut merkittävä muutos. Asiakas on vaihtanut kultansa pnkin sitoumukseen maksaa asiakkaalle kultaa tämän tarvitessa sitä. Ero on syvällinen, vaikka asiakas ei välttämättä sitä edes huomaa.

Koska asiakkaille riittää yleensä lupaus kullasta kullan konkreettisen omistamisen sijaan, voivat pankit “lainata” asiakkaille kultaa vain kasvattamalla summaa, jonka se on luvannut heille tarvittaessa maksaa. Tällä lailla käy nopeasti niin, että pankkitalletuksien yhteenlaskettu kultamäärä ylittää maailmassa oikeasti olemassa olevan kullan määrän moninkertaisesti. Jos kaikki haluaisivat yhtäaikaa kultansa takaisin, ei kultaa riittäisi kuin muutamille ensimmäisille.

Tässä vaiheessa sanalla “raha” tarkoitetaan luultavasti sekä oikeaa kultaa että pankkitalletuksia eli pankin lupauksia rahasta. Koska pankit voivat lainata eli luvata lisää kultaa nin paljon kuin uskaltavat, voivat ne luoda rahaa tässä sanan uudessa merkityksessä.

On sanomattakin selvää, että kyky luoda rahaa tekee pankeista vaikutusvaltaisia toimijoita. Pankkien valtaa reguloi niiden jatkuva vaara joutua vararikkoon—paitsi jos pankki kasvaa niin suureksi, että yhteiskunta alkaa olla liian riippuvainen siitä. Tällöin valtiot ovat halukkaita auttamaan pankkia jos se joutuu ongelmiin, ja tämä taas vähentää pankin vastuuta sen uhkapelaamisesta.

Jos pankit kasvavat tarpeeksi suuriksi, alkavat ne hallita maailmaa valtioiden sijaan. Tämä on kai tapahtumassa (tai on jo tapahtunut). Kuvaava numero voisi olla vaikkapa se, että 20 suurinta pankkia maailmassa hallitsevat jokainen yli tuhannen miljardin dollarin edestä hyödykkeitä (asset). Vertailun vuoksi Yhdysvaltojen kokonaisvarallisuus on luokkaa 80 tuhatta miljardia dollaria.
([lähde 1](https://en.wikipedia.org/wiki/List_of_largest_banks), [lähde 2](https://en.wikipedia.org/wiki/Wealth_in_the_United_States))

Nykyaikaiset valuutat, kuten euro ja dollari, eivät enää pohjaudu kultaan. Ne pohjautuvat keskuspankkirahaan, jonka arvo perustuu keskuspankin lupaukseen vaihtaa se johonkin esoteeriseen velkakirjaan tai johonkin. Käteinen eli paperi- ja kolikkoraha on tällaista keskuspankkirahaa. En ole teknisistä yksityiskohdista aivan täysin perillä, mutta suurinpiirtein tällä lailla se menee.

Kokonaisrahasta—siis siitä, minkä kansa rahaksi mieltää—vain jokunen prosentti on keskuspankkirahaa. Tämän lupauksen luotettavuutta voi kokeilla käymällä pankkiautomaatilla. Yleensä sieltä saa rahaa, mutta ei aina—käsittääkseni esimerkiksi Kreikan pankkikriisissä jokunen vuosi sitten kävi niin, että rahaa ei saanut enää automaatista pihalle.

Bitcoinin kaltaisilla kryptovaluutoilla *saattaa* olla pankkien ylivaltaa purkava vaikutus, sillä kryptovaluutat eivät kärsi kullan epäkäytännöllisyyksistä. Toki kryptovaluutoissa on ongelmia—esimerkiksi Bitcoin kuluttaa niin paljon sähköä että se uhkaa nousta merkittäväksi ympäristöongelmaksi—mutta näitä ongelmia on mahdollista purkaa suunnittelemalla fiksumpia kryptovaluuttoja. Tietysti se, alkaako ihmiset uskoa johonkin fiksuun kryptovaluuttaan, on kysymys, johonka saadaan vastaus vain odottamalla.

(Saakeli, suljin jossain vaiheessa vahingossa typewriter.py:n, piti kirjottaa vimillä loppuun. Pitääpä jotenkin ottaa huomioon tällaset tilanteet ja muuttaa koodia jotenkin.)


---

*2017-11-06 01:17:16.964477*

# Paljonpuhuttu läsnäolo

Kun näkee, että kaikki menee päin vittua ja ihmiskunta ajaa itsensä tuhoon, eikä asialle näytä voivan tehdä mitään, voi kyse olla todella masentavasta kokemuksesta. Näin se itselle ainakin on ollut.

Kuinka päästä näistä fiiliksistä pois? Monesti sanotaan, että pitää oppia päästämään irti neuroottisesta halusta kontrolloida tulevaa ja olla läsnä hetkessä. Kyllä, näin se varmasti on. Mutta tuohon tilaan pääseminen on erittäin, erittäin epätriviaali juttu.

Asiasta on kirjoitettu kirjoja, kuten vaikkapa Eckhart Tollen *Läsnäolon voima*. Olen lukenut sen, ja hetken aikaa uskoin päässeeni kohti tuota mystistä läsnäoloa. Nykyään ajattelen, että kirja kusetti mua, eikä se vienyt mua lähemmäksi mitään mitä voisi kutsua läsnäoloksi.

Ainoa oikeasti toimiva tapa päästä kohti läsnäoloa, mitä perkelettä se sitten tarkoittaakin, on ymmärtää maailmaa. Meditaatio, filosofia ja psykedeeliset huumeet on hyödytömiä siinä asiassa. Tarvitaan kovaa tieteellistä työtä. Pitää käydä läpi dataa—eli käytännössä tietoa ihmiskunnan historiasta—jja malleja—eli käytännössä evoluutioteoriaa.

Tieteellisen työn myötä on mahdollista nähdä ihmiskunta ulkopuolisen silmin, aivan kuin olisi avaruusolio, ja katsoa sitä tyynesti sivusta, olipa se tekemässä mitä tahansa. Omalla kohdallani kirja nimeltä *Sapiens: A Brief History of Humankind* (en muista kirjoittajan nimeä tähän hätään) oli vaikuttava kokemus. Se käy läpi ihmiskunnan historian pari miljoonan vuoden takaa tähän päivään hämmästyttävän neutraalista perspektiivistä, korostaen evolutiivista ajattelua.

Sitä, mitä itse ymmärsin tuota kirjaa lukiessani, on mahdotonta kuvailla tyhjentävästi. (Totta kai monella muullakin kirjalla, keskustelulla, elämänkokemuksella ja vaikka millä on ollut myös vaikutusta. Jostain syystä vain juuri tuota kirjaa lukiessa tapahtui jonkunsortin läpimurto ymmärryksessä. Veikkaan, että se on aika pitkälti sattumaa.) Voin kuitenkin kuvailla jotain ajatuskulkuja joita päässäni on herännyt.

Jos laitamme bakteereja petrimaljaan, kasvaa niiden määrä ensin eksponentiaalisesti. Jossain vaiheessa ne ovat syöneet kaiken ravinnon maljasta. Sitten ne kuolevat. Olenko vihainen bakteereille siitä, että ne olivat niin tyhmiä että ne söivät kaikki resurssinsa? Yritänkö epätoivoisesti varoittaa heitä liiasta ahneudesta?

Bakteerit eivät ole ainoita elollisia olentoja, jotka ovat liian typeriä lopettaakseen syömisen ajoissa. Joka ikinen eläinlaji on sopeutunut elämään sellaisissa olosuhteissa, joissa resursseja on saatavilla liian vähän. Tämä on antanut niille  loputtoman nälän, jota ei voi tyydyttää millään. Jos resursseja on nyt yhtäkkiä saatavilla rajattomasti, käy huonosti.

Ihminen on eläin, ja tämä kaikki pätee ihmiseenkin. Olemme viisaudessamme, tai ainakin älykkyydessämme, oppineet rakentamaan pikaruokaloita, jotka toimivat meille rajattomina kilo- ja megakalorien lähteenä. Kyse on vain iänikuisesta vaistosta, joka sanoo että mitä enemmän energiaa, sen parempi. Nyt tuo vaisto pettää meidät, ja me lihomme ja sairastumme kaiken maailman tauteihin.

Sama ilmiö tapahtuu muidenkin asioiden kohdalla. Rakennamme isompia voimalaitoksia, kaivoksia, korkeampia taloja, nopeampia tietokoneita. Tappavampia aseita. Teemme kaikesta suurempaa, koska olemme miljoonien vuosien ajan oppineet, että se on se tapa, jolla pärjäämme. Downshiftaus ei ole mahdollista; se on vastoin jokaisessa solussamme elävän DNA-molekyylin käskyjä.

Nyt, muutaman viime vuosituhannen, ja erityisesti muutaman vuosisadan aikana petrimaljamme rajat ovat alkaneet tulla vastaan. Olemme aina syöneet Maapalloa, ja nyt se alkaa näkyä. Me syömme ja syömme, ja vaikka kaikki on kuulleet ilmastonmuutoksesta ja ydinsodasta, syömme lisää. Upshiftaamme, koska DNA sanoo niin.

Kohta olemme rakentaneet, keksineet ja syöneet kaikein. Ei ole enää työtä tehtäväksi ja elämää elettäväksi. Petrimalja on kaluttu loppuun. Kuinka tässä voi olla vihainen ihmiskunnalle? Tai katkera, tai surullinen? No, onhan se mahdollista tietenkin. Mutta kun hetken ajan katsoo ihmisiä samasta vinkkelistä kuin bakteereja, voi kokea rauhallisuutta siitäkin huolimatta että tietää kaiken olevan tuhoutumassa.

Tällaisessa valaistumisessa ja läsnäolon saavuttamisessa on se hieno puoli, että se vapauttaa päässä pyörivistä luupeista ja antaa mahdollisuuden tehdä hyödyllistä työtä, mikäli sellaista sattuu jostakin ilmestymään.

---

*2017-11-06 02:26:20.884137*

# Psykopaatin tunnistaminen on aina vaikeaa

Katsoin hiljattain elokuvan *Gone Girl*, jossa esiintyy jonkin sortin psykopaatti. Minulla meni jonkin aikaa hoksata, että kuka se oli. Elokuva oli aika hyvä, koska se esitti psykopaatin ilman mitään empaattisuuden pilkahduksia. Useimmat elokuvat ja tv-sarjat lisäävät psykopaattisiin hahmoihin jonkinlaista kieroa empaattisuutta, joko typeryyttään tai lisätäkseen hahmoon mystisyyttä ja kiinnostavuutta, mutta *Gone Girl* jätti sen tekemättä. Hintana on tietysti se, että psykopaattihahmosta tulee tylsä—mutta se on vain realismia. Psykopaatit ovat tylsiä, jos maskin taakse onnistuu jollain ilveellä kurkistamaan.

Psykopaatin tunnistaminen on vaikeaa. Vuosimiljoonien saatossa psykopaatti on oppinut matkimaan ei-psykopaattista, aitoa ja empaattista ja rehellistä ihmistä lähes kaikissa asioissa, ja puhumaan itsensä mistä tahansa tilanteesta pois. Ei ole olemassa mitään käytösmallia, josta voisi varmaksi sanoa, että kyseessä on psykopaatti.

Tämä tarkoittaa sitä, että yksittäinen havainto ei koskaan ole tarpeeksi. Mikä tahansa altruistiselta näyttävä teko voi olla näytös, ja kuka tahansa altruistinen ihminen voi joskus manipuloida, jos tilanne sitä vaatii ja empatia onnistuu kytkeytymään hetkeksi pois päältä.

Havaintoja onkin tehtävä paljon, ja etsittävä sieltä “big datan” seasta konsistentteja, toistuvia kuvioita. Sekään ei ole helppoa, kun psykopaatti voi häiritä tarkkailua tuottamalla supernormaalia stimulusta.

Tärkein kysymys on, “työskenteleekö hän, jotta minulla olisi hyvä olla?” Ajan kanssa vastaus löytyy. Suurimman osan kohdalla vastaus on selvä kyllä. Joidenkin kohdalla se on selvä ei. Nämä ovat psykopaatteja. Joskus voi vastaan tulla henkilö, joka vuosi toisensa jälkeen näyttää pysyvän välimaastossa. Nämä ovat surkimuksen oloisia kavereita, jotka eivät ole osanneet tehdä sosiaalisbiologista uravalintaansa.

---

*2017-11-06 04:21:26.245288*

# Raha ja käyttötavara

Siinä, missä käyttötavaraa arvosttetaan sen takia, että sillä voi tehdä jotain hyödyllistä, arvostetaan rahaa sen takia, että muiden oletetaan arvostavan sitä. Ero on sama kuin tykkäämisen ja teeske telyn välillä.

Kun esimerkiksi joku ns. oikea muusikko soittaa, päästelee hän soittimestaan sellaisia ääniä, joista hän pitää itse. Hän fiilistelee musiikkia. Kuulijatkin saattavat tfiilistellä, mutta homma lähtee kuitenkin muusikon tunne-elämästä.

Paska muusikko taas ei koe mitään soittaessaan. Hän vain koittaa keksiä, mitä yleisö haluaa kuulla, ja päästelee sitten sellaisia ääniä.

Kun pienyrittäjä tekee juttuaan,  hän 

Ei saakeöi, väsyttää liikaa. En tiiö oliks tässä kauheesti järkee. Vois kattoohuomenna.

---

*2017-11-06 12:14:10.373153*

# Onko kirja valetta?

Olen jo pitkään halunnut kirjoittaa kirjan. Olen myös yrittänyt sitä, mutta toistaiseksi olen saanut pelkkää roskaa aikaan. Nyt minulla pyörii taas mielessäni kirjan kirjoittaminen.

Se, miksi olen kirjoittanut pelkkää roskaa, johtunee pitkälti siitä, että jos päättää kirjoittaa kirjan, tulee helposti kirjoitettua sellaisella asenteella, että kirjasta on tultava valmis ja virheetön paketti. Ikäänkuin viimeinen sana jollakin tavalla.

On aivan päivän selvää, ettei viimeistä sanaa ole olemassakaan. Kun luen tekstiäni, josta paistaa viimeinen sana -asenne läpi, meinaa oksennus lentää. Miten olen voinut olla näin naiivi? Miten olen voinut kuvitella löytäväni sanat, joiden jälkeen ei enää tarvitse sanoa mitään?

Se, että luulee sanojensa olevan viimeisiä, vähentää sitä mahdollisuutta, että ne itseasiassa oikeasti olisivat niin hyviä sanoja, että palavaa tarvetta asian uudelleen muotoilulle ei ole. Niinkuin vanha viisaus sanoo, se joka luulee tietävänsä, ei tiedä.

Välillä mietin, että onko kaikki kirjat tällä tavalla näsäviisaan typeriä puheenvuoroja. On alku, draaman kaari ja loppu. Viimeinen rivi hehkuu kirjailijan itsetietoisuutta. Moni kirja menee selvästi tähän kategoriaan.

Entä loput? Varmasti moni kirja on vain onnistunut huijaamaan minut mukaan kulttiinsa, uskomaan kirjailijan yli-inhimillisyyteen, ja hyväksymään viimeisen sanan kyseenalaistamatta. Joidenkin kirjojen kohdalla olen myöhemmin huomannut, että näin todellakin kävi. Esimerkkinä vaikkapa *Läsnäolon voima*.

Mä luulen, että aina silloin tällöin kirja onnistuu välttämään tämän sudenkuopan. Esimerkiksi Pieter Hintjensin kirjat onnistuvat siinä kohtalaisen hyvin, vaikka *Culture & Empiren* [linkki](http://cultureandempire.com)lopussa sanotaankin:

> There is no 1% and 99%. There is only 100%. We are one planet, one species, and we live or die together.

Tulipa tyhmä olo kun quotetin loistavana pitämäni kirjan lopun vain valittaakseni siitä. (Mut on tuo nyt tyhmästi sanottu, kerta kaikkiaan. Ihmiset jakautuu oikeasti kahtia—ehkä 3&ndash;5% ja loput, kuten Pieter suraavassa kirjassaan tajuaa, ja oikeasti “me” ollaan monta lajia. Ihmistä ei voi erottaa ekosysteemistä. No ihan sama, nyt lähti pahasti sivuraiteille.)

Mutta siis, tarkoitukseni oli sanoa, että jos kirjailija onnistuu ajattelemaan kirjaansa vain näppäränä tapana paketoida yksi hiukan pidempi puheenvuoro, jonka tulevaisuus tulee renderöimään tarpeettomaksi niinkuin minkä tahansa muunkin puheenvuoron, on olemassa mahdollisuus, että kirjasta tulee itseasiassa pragmaattinen ymmärrystä kasvattava työkalu, joka ei vaikuta näsäviisaalta, eikä rakenna kulttia.

Joten tässä on ohjeita mahdollista kirjan kirjoittamista varten: jos huomaat olevasi sellaisissa fiiliksissä, että TÄMÄ on SE kirja, joka ratkaisee kaiken, pidä taukoa. Jos ajattelet neuroottisesti, että onko tässä nyt kaikki, mitä pitää sanoa vai ei, pidä taukoa. Jos huolit, että uskooko lukija sinua, pidä taukoa.

Sitten, kun sinulla on sellainen olo, että tässä nyt on joitakin ajatuksia, joista voisi tulla näppärä paketti kun ne laittaa yhteen, jatka.

---

*2017-11-06 15:48:57.094445*

# Demotaan pekalle kirjotuskonetta

Perkele

:poop:

---

*2017-11-06 16:35:52.063745*

# Kokeillaan uutta featuree

Nyt pitäs jatkuu. Saa nähä meniks kaikki oikein.

---

*2017-11-06 16:53:20.786007*

# Merkillinen raha

Jos minulla on sopivanlainen paperilappunen hallussani, voin mennä kauppaan ja vaihtaa sen ruokaan. Jos minulla on noita lappusia paljon, voin saada muut tekemään palveluksiani itselleni. Jos minulla on niitä valtavasti, voin hallita maailmaa.

Raha tarkoittaa mitä tahansa omistettavissa olevaa asiaa, jolla on arvoa enemmän, kuin mitä sen hyödyllisyydestä voisi päätellä. Mistä tuo arvo sitten tulee? Vastaus on merkillinen: raha saa arvonsa siitä, että sitä arvostetaan.

Tämä kuulostaa idioottimaiselta, mutta on kuitenkin täysin rationaalista toimintaa ainakin yksilön kannalta. Jos 99% kaupunkilaisista arvostaa rahaa, olisi tyhmää olla uskomatta, että rahaa pidetään arvokkaana—jja se, että uskoo muiden pitävän rahaa arvokkaana on sama asia, kuin että itse arvostaa rahaa. Minä en pidä rahasta, mutta se fakta, että setelillä saa kaupasta ruokaa, ei jätä minulle muuta vaihtoehtoa kuin uskoa, että raha on arvokasta.

Maailmassa on monia eri rahoja, kuten vaikkapa euro, dollari, kulta, bitcoin ja jeni. Jokaista näistä voidaan käyttää tavaroiden arvon mittaamiseen, ja toisessa rahassa ilmoitettu arvo voidaan kääntää toiseksi kurssi-nimisellä kertoimella. Esimerkiksi bitcoinin kurssi on tällähetkellä noin 6000 euroa.

Tämä tarkoittaa tavan tallaajalle sitä, että yhden bitcoinin saa itselleen 6000 eurolla. Miksi? Mitä ylipäänsä valuuttojen kurssit ja markkina-arvot ylipäänsä tarkoittaa?

Tämän ymmärtämiseksi on ensin todettava, että kaupankäynti on nykypäivän maailmassa edelleen pohjimmiltaan vaihtokauppaa. Minulla on jotakin ja sinulla on jotakin. Mitä jos minä annan omani sinulle ja sinä omasi minulle?

Lähtökohtaisesti vaihtokaupassa on hankala sanoa, että kuinka monta omenaa vastaa säkillistä banaaneja. Se riippuu aivan siitä, millä tavalla kaupankäyjät ajattelevat hedelmistä. Mutta jos tuollaisia kauppoja tehdään paljon, ja kaikki ovat tietoisia tostensa kaupoista, löytyy pian joku yleispätevä kurssi jolla ihmiset vaihtavat banaaneja omenoihin. Tämä johtuu siitä, että jos joku ei suostu myymään tarpeeksi halvalla, menee ostaja toisen kauppiaan luo. Ja jos joku ei suostu ostamaan tarpeeksi halvalla, menee kauppias toisen ostajan luo. Kysyntä, tarjonta, tiedon välitys ja tiuhaan käyvä kauppa takaavat sen, että melkein kaikki kaupat tiettynä ajanhetkenä käydään aina suurinpiirtein samalla kurssilla.

Omenien ja banaanien välinen kurssi ei välttämättä koskaan saavuta kovin tarkasti vakiintunutta arvoa, mutta esimerkiksi bitcoinin ja euron välillä tilanne on toinen. Niiden välistä kauppaa käydään todella tiuhaan, ja kaikki bitcoinit ovat keskenään identtisiä, ja kaikki eurot ovat keskenään identtisiä. Niinpä bitcoinin ja euron välisestä kurssista voidaan puhua useamman numeron tarkkuudella.

Kurssit kehittyvät ajan myötä kun ihmisten suhteellinen arvostus valuuttoja kohtaan muuttuu. Tämä johtuu siitä, että ihmiset tietävät, että kurssit saattavat muuttua. Jos henkilö olettaa bitcoinin kurssin nousevan huomenna, hän itse arvostaa sitä jo tänään enemmän. Kun moni ihminen ajattelee näin, nousee kurssi, joka siis ei tarkoittanut muuta kuin keskimääräistä hintaa jolla kauppaa käydään.

Kuulostaa jotenkin kehämäiseltä toiminnalta, mutta niin se menee. Ihmiset arvostavat valuuttaa sen mukaan, miten he olettavat muiden arvioivan valuuttaa tulevaisuudessa. Kun jokainen arvioi muiden arvioimista, syntyy kaoottinen feedback-luuppi ja kurssit pomppivat miten sattuu.

Se, että kurssit muuttuvat, tekee myös arvon mittaamisesta hankalaa. Missä valuutassa mittaisin arvoa? Mikä on tavaran arvo, jos sitä ei ole kaupan tarpeeksi paljoa, jotta voisimme sanoa jotain tyypillisestä tai keskimääräisestä hinnasta? Kuinka arvottaa asioitta vuosien takaa? Kuinka monta vuoden 1952 dollaria vastaa vuoden 2017 dollareita? Tähän kysymykseen ei oikeasti voi vastata mitenkään, koska on loogisesti täysin mahdotonta käydä kauppaa kahden eri ajan välillä.

Tämän ei sinänsä pitäisi yllättää. Miksi hyödykkeiden arvoa pitäisi kyetä mittaamaan numeerisesti? Se, miten ihmiset asioita arvostavat, riippuu niin monesta asiasta—siitä mitä henkilöllä on, mitä muilla on, minkälaisia uskomuksia henkilöllä on, minkälaisessa mielentilassa henkilö on, ja niin edelleen—että monesti numeerisen arvon määrittäminen ei ole muuta kuin aivan järjetöntä puuhaa.

Toki käytännössä monella tuotteella on hyvinmääritelty markkina-arvo siinä valuutassa, mikä on kaikista vakiintunein aluueella jossa ollaan. Kaljatölkki maksaa euron ja sillä sipuli. Tämä aiheuttaa sen, että ihmiset alkavat arvottaa kaikkea rahan mukaan: teenkö mieluummin työkeikan vai vietänkö kavereideni kanssa aikaa? Hlausipa sitä tai ei, on tämän kysymyksen pohtiminen ystävyyden rahallisen arvon pohtimista.

Työmarkkinat mittaavat ihmisen aikaa rahassa. Täällä Suomessa tavallisen ihmisen aika maksaa 10–20 euroa per tunti. Rikkaan yrittäjän aika taas saattaa olla arvoltaan vaikka 10–20 euroa per minuutti, jos ajan arvo lasketaan voitosta, jonka yrittäjä siinä tekee. Työttömän arvo taas on negatiivinen, kun yhteiskunta joutuu maksamaan tälle toimeentulotukea.

Rahaa ei voi päästä karkuun—sen syntymiseen riittää käsitys omistusoikeudesta ja vaihtokaupasta—mutta saatanan paskoja asioita se aiheuttaa. Se, että toisen ihmisen arvo on negatiiven ja toisen tähtitieteellinen on vain aivan järjetöntä, varsinkin kun tuo arvoltaan negatiivinen henkilö voi hyvinkin vaikka kasvattaa lasta samalla kun tähtitieteellisen arvon omaava henkilö tuhoaa kansanterveyttä ja luontoa—tai jopa ylläpitää verta, kipua ja kuolemaa aiheuttavia sotia.

Kaikista ikävin juttu on se, että raha—ja mikä tahansa muukin omaisuus—tuppaa menemään sinne, missä sitä ja on. Omaisuus keskittyy ja valtakin keskittyy siinä sivussa. Kukaan ei tunnu ymmärtävän kuinka kokonaisvaltaisesta asiasta tässä on kyse. Kohta yksi ihminen omistaa kaikein.

Tavallaan tämä on aivan ymmärrettävää. Einstein, joka keksi suhteellisuusteorian, ei voinut uskoa, että maailmankaikkeus muuttuu, vaikka tuo muuttuminen on suhteellisuusteorian suora seuraus, josta Einstein oli tietoinen. Hän yritti “pelastaa” tilanteen lisäämällä teoriaan ylimääräisen numeron nimeltään kosmologinen vakio. Vasta Hubblen (oliko se Hubble?) eksplisiittinen havainto siitä, että maailmankaikkeus laajenee sai Einsteinin avaamaan silmänsä.

Samalla tavalla meillä yhteiskunnassa moni ymmärtää sen ilmiömaailman, joka johtaa väistämättä vaurauden keskittymiseen yhdelle henkilölle, mutta silti on kykenemätön uskomaan tuota seurausta. Ajan myötä se käy kyllä selväksi.
