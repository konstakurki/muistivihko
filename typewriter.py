#!/usr/bin/env python3

import datetime
import subprocess
import random
import os

gitfeatures = True

filename = 'README.md'

spchrs =   {'¬':'—',
            '\\n-':'–',
            '\\...':'…'}

def writeline(string):
    with open(filename,'a') as f:
        f.write(string)

def escape(string):
        pholder = str(random.random())
        string = string.replace('\\\\',pholder)
        for i in spchrs:
            string = string.replace(i,spchrs[i])
        string = string.replace(pholder,'\\')
        return string

def writedate():
    writeline('\n---\n\n*'+str(datetime.datetime.now())+'*\n\n')

def main():
    if gitfeatures:
        try:
            subprocess.check_output(['git','pull'])
        except:
            pass
    print('\a')
    writedate()
    line = input('')
    cmessage = escape(line)
    while True:
        if line == '\\quit':
            if gitfeatures:
                subprocess.check_output(['git','add',filename])
                subprocess.check_output(['git','commit','-m',cmessage])
                try:
                    subprocess.check_output(['git','push'])
                except:
                    pass
            print('\a')
            break
        elif line == '\\beep':
            print('\a')
        elif line == '\\specialcharacters':
            for i in spchrs:
                print('   {0}    {1}'.format(spchrs[i],i))
        elif line[0:6] == '\\code.':
            fname = str(random.random()) + line[1:]
            subprocess.check_call(['vim',fname])
            with open(fname) as f:
                newline = f.read()
            os.remove(fname)
            while newline.find('\n\n') != -1:
                newline = newline.replace('\n\n','\n')
            writeline(newline)
        else:
            writeline(escape(line)+'\n')
        line = input('')

if __name__ == '__main__':
    main()
